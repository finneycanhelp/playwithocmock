//
//  PlayWithOCMockTests.m
//  PlayWithOCMockTests
//
//  Created by Michael Finney on 2/10/13.
//  Copyright (c) 2013 Michael Finney. All rights reserved.
//

#import "PlayWithOCMockTests.h"
#import <OCMock/OCMock.h>
#import "OCViewController.h"
#import "OCClient.h"

@implementation PlayWithOCMockTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExampleOfSettingUpMockAndExpectingAMethodToGetCalled
{

    // this depends on the client being created through lazy initialization and a public property
    
    OCViewController *controller = OCViewController.new;
    
    id mockClient = [OCMockObject mockForClass:[OCClient class]]; [OCMockObject mockForClass:[OCClient class]];
    controller.client = mockClient;
    
    [[mockClient expect] callSomeFarOffServer];
    
    [controller hitMeTapped:nil];
   
    [mockClient verify];
    
//    STFail(@"Unit tests are not implemented yet in PlayWithOCMockTests");
}

@end
