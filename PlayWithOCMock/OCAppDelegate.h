//
//  OCAppDelegate.h
//  PlayWithOCMock
//
//  Created by Michael Finney on 2/10/13.
//  Copyright (c) 2013 Michael Finney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
