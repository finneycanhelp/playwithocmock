
#import "OCViewController.h"
#import "OCClient.h"

@implementation OCViewController

- (OCClient *)client
{
    if (!_client) {
        _client = OCClient.new;;
    }
    
    return _client;
}

- (IBAction)hitMeTapped:(id)sender {
    
     [self.client callSomeFarOffServer];
    
}

@end
