//
//  main.m
//  PlayWithOCMock
//
//  Created by Michael Finney on 2/10/13.
//  Copyright (c) 2013 Michael Finney. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OCAppDelegate class]));
    }
}
