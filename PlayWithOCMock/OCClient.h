//
//  OCClient.h
//  PlayWithOCMock
//
//  Created by Michael Finney on 2/10/13.
//  Copyright (c) 2013 Michael Finney. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OCClient : NSObject

- (void)callSomeFarOffServer;

@end
