
#import <UIKit/UIKit.h>
#import "OCClient.h"

@interface OCViewController : UIViewController


// in .h for unit testing
@property (nonatomic) OCClient *client;

- (IBAction)hitMeTapped:(id)sender;

@end
